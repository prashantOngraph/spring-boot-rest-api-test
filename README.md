# Spring Boot Rest API + Docker compose + Mockito Test

This repository will provide basic implementation of REST API with `Spring Boot Framework on Groovy` with `Mockito framework` testing and along with `Docker` compose.

**What is Docker?**

Docker is a set of platform as a service products that uses OS-level virtualization to deliver software in packages called containers.

**Dockerfile**

`FROM openjdk:8-jdk-alpine`<br>
`ADD build/libs/poc-0.0.1-SNAPSHOT.jar poc-app.jar`<br>
`EXPOSE 8080`<br>
`ENTRYPOINT ["java","-jar","poc-app.jar"]`<br>

`From`<br> _It tells docker, from which base image you want to base your image from. In our example, we are creating an image from the ubuntu image_

`Add`<br> _It tells docker to run fully compiled application from jar file which is mentioned_ <br>

`EXPOSE`<br> _Docker will expose port number on which application will be running_ <br>

`ENTRYPOINT` <br> _Allows you to configure a container that will run as an executable_ <br>

1. `Docker-compose build` <br> _It will build latest docker image_
2. `Docker-compose up`  <br> _It will up docker application using_ `docker-compose.yml` `file`
3. `Docker-compose down` <br> _It will down docker application_

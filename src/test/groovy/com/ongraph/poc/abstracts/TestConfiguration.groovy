package com.ongraph.poc.abstracts

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.ongraph.poc.DTO.ResponseDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.ResultHandler
import org.springframework.test.web.servlet.ResultMatcher
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

abstract class TestConfiguration {

    @Autowired
    MockMvc mockMvc

    ObjectMapper objectMapper

    ResponseDTO getResponseDTO(MvcResult result) {
        return objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class)
    }

    String convertObjectToJsonString(def object) throws JsonProcessingException {
        objectMapper = new ObjectMapper()
        return objectMapper.writeValueAsString(object)
    }

    ResultHandler print() {
        return MockMvcResultHandlers.print()
    }

    ResultMatcher statusOk() {
        return MockMvcResultMatchers.status().isOk()
    }
}

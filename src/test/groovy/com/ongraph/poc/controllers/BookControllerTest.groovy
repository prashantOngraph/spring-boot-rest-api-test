package com.ongraph.poc.controllers

import com.ongraph.poc.CO.BookCO
import com.ongraph.poc.abstracts.TestConfiguration
import com.ongraph.poc.constants.MessageConstants
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class BookControllerTest extends TestConfiguration {


    @Test
    void testCreateBook() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/book/add")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(convertObjectToJsonString(new BookCO(title: "ascasc", price: 45.3, id: null))))

                .andDo(print())
                .andExpect(statusOk())
                .andExpect(MockMvcResultMatchers.jsonPath('$.data').value("Book has been saved"))
    }

    @Test
    void testBookFindById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/book/2")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))

                .andDo(print())
                .andExpect(statusOk())
                .andExpect(MockMvcResultMatchers.jsonPath('$.data.title').value("Groovy"))
    }

    @Test
    void testBookUpdateById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/book/update/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(convertObjectToJsonString(new BookCO(title: "Action in java", price: 250))))

                .andDo(print())
                .andExpect(statusOk())
                .andExpect(MockMvcResultMatchers.jsonPath('$.data').value(MessageConstants.BOOK_UPDATED))
    }

    @Test
    void testBookFindAll() {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/book/list")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))

                .andDo(print())
                .andExpect(statusOk())
                .andExpect(MockMvcResultMatchers.jsonPath('$.data.length()').value(7))
    }

    @Test
    void testBookDelete() {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/book/delete/18")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))

                .andDo(print())
                .andExpect(statusOk())
                .andExpect(MockMvcResultMatchers.jsonPath('$.data').value(MessageConstants.BOOK_DELETED))
    }
}

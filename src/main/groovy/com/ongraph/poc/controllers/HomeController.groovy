package com.ongraph.poc.controllers

import com.ongraph.poc.DTO.ResponseDTO
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/")
class HomeController {

    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    ResponseEntity<ResponseDTO<String>> index(String fullName) {
        ResponseDTO<String> responseDTO = new ResponseDTO<>()
        fullName = "Welcome ${fullName} to groovy project in spring boot"
        responseDTO.setSuccessResponse(fullName)
        return ResponseEntity.ok(responseDTO)
    }
}

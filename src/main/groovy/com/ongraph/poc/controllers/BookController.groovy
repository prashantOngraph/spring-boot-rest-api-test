package com.ongraph.poc.controllers

import com.ongraph.poc.CO.BookCO
import com.ongraph.poc.DTO.ResponseDTO
import com.ongraph.poc.domain.Book
import com.ongraph.poc.services.BookService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/book")
class BookController {

    @Autowired
    BookService bookService

    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ResponseDTO<String>> save(@RequestBody BookCO bookCO) {
        ResponseDTO<String> responseDTO = bookService.save(bookCO)
        return ResponseEntity.ok(responseDTO)
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ResponseDTO<String>> update(@RequestBody BookCO bookCO, @PathVariable Long id) {
        ResponseDTO<String> responseDTO = bookService.update(bookCO, id)
        return ResponseEntity.ok(responseDTO)
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    ResponseEntity<ResponseDTO<Book>> findById(@PathVariable Long id) {
        ResponseDTO<Book> responseDTO = bookService.findById(id)
        return ResponseEntity.ok(responseDTO)
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    ResponseEntity<ResponseDTO<List<Book>>> findAll() {
        ResponseDTO<List<Book>> responseDTO = bookService.fetchBooks()
        return ResponseEntity.ok(responseDTO)
    }

    @RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    ResponseEntity<ResponseDTO<String>> delete(@PathVariable Long id) {
        ResponseDTO<String> responseDTO = bookService.delete(id)
        return ResponseEntity.ok(responseDTO)
    }
}

package com.ongraph.poc.constants

interface MessageConstants {

    String BOOK_SAVED = "Book has been saved"
    String NO_BOOK = "No book found"
    String BOOK_UPDATED = "Book has been updated"
    String BOOK_DELETED = "Book has been deleted"
}
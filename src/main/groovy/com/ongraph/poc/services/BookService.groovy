package com.ongraph.poc.services

import com.ongraph.poc.CO.BookCO
import com.ongraph.poc.DTO.ResponseDTO
import com.ongraph.poc.constants.MessageConstants
import com.ongraph.poc.domain.Book
import com.ongraph.poc.repository.BookRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BookService {

    private static final Logger log = LoggerFactory.getLogger(BookService.class)


    @Autowired
    BookRepository bookRepository

    ResponseDTO<String> save(BookCO bookCO) {
        log.info("...init saving book")
        ResponseDTO<String> responseDTO = new ResponseDTO<>()
        Book book = new Book(title: bookCO.title, price: bookCO.price)
        bookRepository.save(book)
        responseDTO.setSuccessResponse(MessageConstants.BOOK_SAVED)
        log.info("...${bookCO.title} has been saved")
        return responseDTO
    }

    ResponseDTO<String> update(BookCO bookCO, Long id) {
        ResponseDTO<String> responseDTO = new ResponseDTO<>()
        Optional<Book> bookOptional = bookRepository.findById(id)
        if (!bookOptional.isPresent()) {
            responseDTO.setFailureResponse(MessageConstants.NO_BOOK)
        } else {
            Book book = bookOptional.get()
            book.title = bookCO.title
            book.price = bookCO.price
            bookRepository.save(book)
            responseDTO.setSuccessResponse(MessageConstants.BOOK_UPDATED)
        }
        return responseDTO
    }

    ResponseDTO<Book> findById(Long id) {
        ResponseDTO<Book> responseDTO = new ResponseDTO<>()
        Optional<Book> bookOptional = bookRepository.findById(id)
        if (!bookOptional.isPresent()) {
            responseDTO.setFailureResponse(null)
        } else {
            responseDTO.setSuccessResponse(bookOptional.get())
        }
        return responseDTO
    }

    ResponseDTO<List<Book>> fetchBooks() {
        ResponseDTO<List<Book>> responseDTO = new ResponseDTO<>()
        List<Book> bookList = bookRepository.findAll()
        responseDTO.setSuccessResponse(bookList)
        return responseDTO
    }

    ResponseDTO<String> delete(Long id) {
        ResponseDTO<String> responseDTO = new ResponseDTO<>()
        Optional<Book> bookOptional = bookRepository.findById(id)
        if (!bookOptional.isPresent()) {
            responseDTO.setFailureResponse(MessageConstants.NO_BOOK)
        } else {
            bookRepository.delete(bookOptional.get())
            responseDTO.setSuccessResponse(MessageConstants.BOOK_DELETED)
        }
        return responseDTO
    }
}

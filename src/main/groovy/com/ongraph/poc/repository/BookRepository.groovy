package com.ongraph.poc.repository

import com.ongraph.poc.domain.Book
import org.springframework.data.jpa.repository.JpaRepository

interface BookRepository extends JpaRepository<Book, Long> {

}
package com.ongraph.poc

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class PocApplication {

	static void main(String[] args) {
		SpringApplication.run(PocApplication, args)
	}

}

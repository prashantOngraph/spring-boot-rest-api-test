package com.ongraph.poc.DTO

class ResponseDTO<T> {

    boolean status
    T data

    void setSuccessResponse(T data) {
        this.status = true
        this.data = data
    }

    void setFailureResponse(T data) {
        this.status = false
        this.data = data
    }

}

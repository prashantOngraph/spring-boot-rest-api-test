FROM openjdk:8-jdk-alpine
ADD build/libs/poc-0.0.1-SNAPSHOT.jar poc-app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","poc-app.jar"]
